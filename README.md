# codility-OddOccurencesInArray

This is the third lesson in Codility; that is given an array, find a value that has no duplication. I scored 100% for it.